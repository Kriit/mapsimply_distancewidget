import { LightningElement,api,track } from 'lwc';
import getRecordDetail from '@salesforce/apex/DistanceWidgetCon.getRecordDetail';

export default class DistanceWidget extends LightningElement {
    @api googleMapAPIKey;
    @api recordId;
    @track recordDetails;

    @api fromStreet;
    @api fromCity;
    @api fromState;
    @api fromPostalCode;
    @api fromCountry;
    
    @api toStreet;
    @api toCity;
    @api toState;
    @api toPostalCode;
    @api toCountry;

    @track vfpageurl;
    
    @api minHeight = '300';

    get height() {
        return `min-height: ${this.minHeight}px`;
    }

    connectedCallback(){
        console.log('------DistanceWidget==connectedCallback------');
        console.log('------googleMapAPIKey------',this.googleMapAPIKey);
        const addressFields = [];
        addressFields.push(this.fromStreet);
        addressFields.push(this.fromCity);
        addressFields.push(this.fromState);
        addressFields.push(this.fromPostalCode);
        addressFields.push(this.fromCountry);
        addressFields.push(this.toStreet);
        addressFields.push(this.toCity);
        addressFields.push(this.toState);
        addressFields.push(this.toPostalCode);
        addressFields.push(this.toCountry);
        console.log('------addressFields------',addressFields);
        getRecordDetail({recordId: this.recordId, addressFields : JSON.stringify(addressFields)}).then(result=>{
            console.log('result',result);
            if(result.length > 0){
                this.recordDetails = result[0];

                let fromAddress = [];
                if(this.recordDetails[this.fromStreet])
                    fromAddress.push(this.recordDetails[this.fromStreet]);
                if(this.recordDetails[this.fromCity])
                    fromAddress.push(this.recordDetails[this.fromCity]);
                if(this.recordDetails[this.fromState])
                    fromAddress.push(this.recordDetails[this.fromState]);
                if(this.recordDetails[this.fromPostalCode])
                    fromAddress.push(this.recordDetails[this.fromPostalCode]);
                if(this.recordDetails[this.fromCountry])
                    fromAddress.push(this.recordDetails[this.fromCountry]);
                let originAddress = fromAddress.join(', ');
                console.log('originAddress',originAddress);

                let toAddress = [];
                if(this.recordDetails[this.toStreet])
                    toAddress.push(this.recordDetails[this.toStreet]);
                if(this.recordDetails[this.toCity])
                    toAddress.push(this.recordDetails[this.toCity]);
                if(this.recordDetails[this.toState])
                    toAddress.push(this.recordDetails[this.toState]);
                if(this.recordDetails[this.toPostalCode])
                    toAddress.push(this.recordDetails[this.toPostalCode]);
                if(this.recordDetails[this.toCountry])
                    toAddress.push(this.recordDetails[this.toCountry]);
                let destinationAddress = toAddress.join(', ');
                console.log('destinationAddress',destinationAddress);

                this.vfpageurl = '/apex/DistanceWidget?key='+this.googleMapAPIKey+'&origins='+originAddress+'&destinations='+destinationAddress;
            }
        }).catch(error=>{
            console.log('error',error);
        });
    }
}