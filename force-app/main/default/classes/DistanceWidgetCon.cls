public class DistanceWidgetCon {
    @AuraEnabled
    public static Object getRecordDetail(String recordId, String addressFields){
        try{
            System.debug('=====DistanceWidgetCon==getRecordDetail=====');
            String fromObject = String.valueOf(Id.valueOf(recordId).getSobjectType());
            System.debug('=====fromObject====='+fromObject);
            System.debug('=====addressFields====='+addressFields);
            Set<String> setofAddressFields = (Set<String>)JSON.deserialize(addressFields, Set<String>.class);
            System.debug('=====setofAddressFields====='+setofAddressFields);
            String selectClause = String.join(new List<String>(setofAddressFields), ',');
            System.debug('=====selectClause====='+selectClause);
            
            String queryString = 'SELECT '+selectClause+' FROM '+fromObject+' WHERE Id =: recordId';
            System.debug('=====queryString====='+queryString);
            return Database.query(queryString);
        }catch (Exception e){
            if(!test.isrunningtest())
                throw new AuraHandledException(e.getMessage());
            return null;
        }
    }
}