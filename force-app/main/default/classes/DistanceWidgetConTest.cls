@isTest
public class DistanceWidgetConTest{
    static testMethod void uintTest1(){
        Account acc = new Account();
        acc.Name = 'Distance Widget';
        acc.Type = 'Prospect';
        acc.Industry = 'Telecommunications';
        acc.BillingStreet = '1449 South Beverly Drive';
        acc.BillingCity = 'Los Angeles';
        acc.BillingState = 'CA';
        acc.BillingPostalCode = '90035-3009';
        acc.BillingCountry =  'United States';
        acc.ShippingStreet = '1450 South Beverly Drive';
        acc.ShippingCity = 'Los Angeles';
        acc.ShippingState = 'CA';
        acc.ShippingPostalCode = '90035-3009';
        acc.ShippingCountry =  'United States';
        insert acc;

        set<String> addressFields = new set<String>();
        addressFields.add('BillingStreet');
        addressFields.add('BillingCity');
        addressFields.add('BillingState');
        addressFields.add('BillingPostalCode');
        addressFields.add('BillingCountry');
        addressFields.add('ShippingStreet');
        addressFields.add('ShippingCity');
        addressFields.add('ShippingState');
        addressFields.add('ShippingPostalCode');
        addressFields.add('ShippingCountry');
        DistanceWidgetCon.getRecordDetail(String.valueOf(acc.Id), JSON.serialize(addressFields));
        DistanceWidgetCon.getRecordDetail(String.valueOf(acc.Id), '');
        System.assertEquals(acc.Industry,'Telecommunications','Error'); 
    }
}